# Общее собрание московского отделения 26.05.2019

Здесь лежат бюллетени и результаты выборов с общего собрания московского
отделения. Файлы в формате [TSV][tsv], чтобы их было легко и в Excel'е открыть,
и программно прочитать: в R для этого подойдёт функция
`read.csv(file = "file.tsv", sep = "\t")`.

Файлы тут следующие:
- `*_ballots.tsv` — это бюллетени. Каждый столбец соответствует кандидату,
а каждая строка — бюллетеню. Галочки в бюллетенях отмечены единицами.
- `*_accepted_results.tsv` — это результаты выборов, которые утвердило общее
собрание.
- `*_recount_results.tsv` — это результаты выборов, полученные при пересчёте
оцифрованных бюллетеней.

[tsv]: https://en.wikipedia.org/wiki/Tab-separated_values
