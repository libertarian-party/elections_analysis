const addTableHeaderCellClickListener = (table) => {
  const headCells = Array.from(
    table.getElementsByClassName('table__header-cell')
  )

  if (headCells.length > 0) {
    const sortTableByNthCol = tableSortByNthColFunc()

    table.addEventListener('click', (e) => {
      if (headCells.includes(e.target)) {
        sortTableByNthCol(table, headCells.indexOf(e.target))
      }
    })
  }
}

const tableSortByNthColFunc = () => {
  let isAsc = true

  return (table, colNum) => {
    Array.from(table.getElementsByClassName('table__row'))
      .sort(tableRowsCompareFunc(colNum, isAsc))
      .forEach((row) => table.appendChild(row))

    isAsc = !isAsc
  }
}

const tableRowsCompareFunc = (colNum, isAsc) => {
  return (row, otherRow) => {
    const cell = getNthCellContent(row, colNum)
    const otherCell = getNthCellContent(otherRow, colNum)

    return isAsc ? compareCells(cell, otherCell) : compareCells(otherCell, cell)
  }
}

const getNthCellContent = (row, cellNum) => {
  const rowCells = Array.from(row.getElementsByClassName('table__cell'))
  return rowCells[cellNum].innerText || rowCells[cellNum].textContent
}

const compareCells = (cell, otherCell) => {
  if (cell !== '' && otherCell !== '' && !isNaN(cell) && !isNaN(otherCell)) {
    return cell - otherCell
  } else {
    return cell.toString().localeCompare(otherCell)
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const tables = Array.from(document.getElementsByClassName('table'))

  if (tables.length > 0) {
    tables.forEach((tbl) => addTableHeaderCellClickListener(tbl))
  }
})
