# This script performs hierarchical clustering of ballots and produces
# dendrograms into delegates.png, regional-committee.png
# and regional-control-commission.png.
# source() this script to your R session or run it with Rscript.

colors <- c("#da4ba4", "#069c32", "#ff8400", "#0657ff", "#c71111")

colorizeLabelsFunc <- function(cuthc, colors) {
  function(node) {
    if (is.leaf(node)) {
      nodeAttrs <- attributes(node)
      col <- colors[cuthc[which(names(cuthc) == nodeAttrs$label)]]
      attr(node, "nodePar") <- c(nodeAttrs$nodePar, lab.col = col)
    }
    node
  }
}


# Delegates
del <- read.csv(file = "../data/delegates_ballots.tsv", sep = "\t")
del.hc <- hclust(dist(cor(del)))
del.cuthc <- cutree(del.hc, 5)
del.colorizeLabels <- colorizeLabelsFunc(del.cuthc, colors)
del.dendr <- dendrapply(as.dendrogram(del.hc), del.colorizeLabels)

png(file = "delegates.png", width = 1200, height = 600, pointsize = 16)
plot(del.dendr, main = "Делегаты")


# Regional committee
rc <- read.csv(file = "../data/regional_committee_ballots.tsv", sep = "\t")
rc.hc <- hclust(dist(cor(rc)))
rc.cuthc <- cutree(rc.hc, 4)
rc.colorizeLabels <- colorizeLabelsFunc(rc.cuthc, colors)
rc.dendr <- dendrapply(as.dendrogram(rc.hc), rc.colorizeLabels)

png(file = "regional-committee.png", width = 800, height = 600, pointsize = 16)
plot(rc.dendr, main = "Руководящий комитет")


# Regional control commission
rcc <- read.csv(
  file = "../data/regional_control_commission_ballots.tsv",
  sep = "\t"
)
rcc.hc <- hclust(dist(cor(rcc)))
rcc.cuthc <- cutree(rcc.hc, 3)
rcc.colorizeLabels <- colorizeLabelsFunc(rcc.cuthc, colors)
rcc.dendr <- dendrapply(as.dendrogram(rcc.hc), rcc.colorizeLabels)

png(
  file = "regional-control-commission.png",
  width = 800,
  height = 600,
  pointsize = 16
)
plot(rcc.dendr, main = "Контрольно-ревизионная комиссия")
