# Анализ внутрипартийных выборов

Директория public содержит исходники сайта. В остальных хранятся данные
о выборах (результаты, бюллетени) и скрипты с анализом этих данных. Названия
директорий соответствуют датам проведения выборов.

Всё содержимое этих [репозитория][repo] и [сайта][website] передано в публичное
достояние, код по условиям [Unlicense](UNLICENSE.txt), другой контент
в соответствии с [CC0][cc0].

[cc0]: https://creativecommons.org/publicdomain/zero/1.0/
[repo]: https://gitlab.com/libertarian-party/elections_analysis
[website]: https://libertarian-party.gitlab.io/elections_analysis
